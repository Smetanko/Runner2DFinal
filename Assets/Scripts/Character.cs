﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Boo.Lang;



    public class Cash : MonoBehaviour {
    public Text txt;
    [SerializeField]
    public int CashChar;

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Cash")
        {
            CashChar=CashChar+1;
        }
        
    }
}



public class Unit : Cash
{

    public virtual void ReceiveDamage()
    {
        Die();
    }

    public virtual void Die()
    {

        Destroy(gameObject);
    }
}



public class Character : Unit
{
    [SerializeField] // отображение значений в графе в unity
    private int lives = 5; // жизни персонажа
    [SerializeField]
    private float speed = 10.0f; // скорость персонажа
    [SerializeField]
    private float JumpForce = 1.0f; // сила прыжка
    // Далее описываем ссылки на компоненты 
    private int gears=1;
    public bool _jump;

    private CharState State
    {
        get { return (CharState) animator.GetInteger("State"); } // gthtjghtltktybt, возвращает текущий state в аниматоре
        set { animator.SetInteger("State", (int)value); }// записывает в аниматор знаяения (куда хотим записать, что хотим записать)


    }
    new private Rigidbody2D rigidbody;
    private Animator animator; // ссылка на компонент аниматор
    private SpriteRenderer sprite; // при движении объекта в противоположную сторону, необходимо будет делать инверсию по горизонтали( поворот персонажа) 
    // будет использовано свойство flip
    private bool isGrounded = false; // для проверки объекта на земле

    private void Awake() // получение ссылок на эти компоненты
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void FixedUpdate() //  метод вызывается для всего что использует физику, вызывается фиксированное количество раз
    {
        CheckGround();
    }
    private void Update()
    {
        //txt.Text = "CashChar: " + CashChar;
        if (isGrounded) State = CharState.CharacterAnim;
        State = CharState.CharacterAnim;// обнуление каждый кадр
        /*if (Input.GetButton("Horizontal"))*/ Run();
       if (isGrounded && Input.GetButtonDown("Jump")) Jump();
        if (isGrounded && _jump)
        {
            rigidbody.AddForce(transform.up * JumpForce, ForceMode2D.Impulse);
        }
        
                //if (transform.position.y < -6) Stop();

    }
    private void Run()
    {
        speed = (speed+Time.deltaTime);
        // Vector3 direction = transform.right * Input.GetAxis("Horizontal"); // направление движения пресонажа
        Vector3 direction = transform.right * speed;
        transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, speed * Time.deltaTime); //MoveTowards- движение вперед (current- текущая позиция,target- цель,maxDistanceDelta-расстояние за один фрейм) строчка будет двигать персонаж
        sprite.flipX = direction.x < 0.0f; // сравнивает значение x  и меняет его на противоположный. Персонаж поворачивается в обратную стороную. Работа с методом Flip по X
        //State = CharState.Run;
        if (isGrounded) State = CharState.Run;// если на земле, то проигрывать анимацию Run
    }
    private void Jump()
    {
        //rigidbody.AddForce(new Vector2(0, 9000));
        rigidbody.AddForce(transform.up * JumpForce, ForceMode2D.Impulse); // задаем прыжок персонажа.(что будет, с какой силой)
        State = CharState.Jump;
    }
    public void OnPress()
    {
        _jump = true;
    }

    public void OnRelease()
    {
        _jump = false;
        rigidbody.AddForce(transform.up*0, 0);
    }
    private void Stop()
    {
        Vector3 direction = transform.right * 0;
    }
    private void CheckGround() // проверка, находится ли перс. на земле
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, 1.0f); // проверка коллайдера игрока на соприкосновение с землей 
        isGrounded = colliders.Length > 1;
        if (!isGrounded) State = CharState.Jump;
    }

   void OnTriggerEnter2D (Collider2D other)
    {
        txt.text = "Gears: " + gears;
        if (other.tag == "Cash")
        {
            gears += 1;
            Destroy(other.gameObject);
        }
    }

}

public enum CharState
{
    CharacterAnim,
    Run,
    Jump,
    BlockAnimation,

}