﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    public Character character;
    private Vector3 lastPosition;
    private float distanceToMove;


    // Use this for initialization
    void Start()
    {
        character = FindObjectOfType<Character>();
        lastPosition = character.transform.position;
    }
	// Update is called once per frame
	void Update () {

        distanceToMove = character.transform.position.x - lastPosition.x;
        transform.position = new Vector3(transform.position.x + distanceToMove, transform.position.y, transform.position.z);

        lastPosition = character.transform.position;
	}
}
