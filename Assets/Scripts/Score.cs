﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{


      public int ScoreChar;
       public Text txt;
       public bool onDeath;
       private float timer;


       // Use this for initialization
       void Start () {

       }

       // Update is called once per frame
       void Update () {
           txt.text = "Score: " + ScoreChar;
           if (onDeath == false)
           {
               timer += 1 * Time.deltaTime;
               if (timer > 1)
               {
                   ScoreChar += 1;
                   timer = 0;
               }
           }
       }
   }
  