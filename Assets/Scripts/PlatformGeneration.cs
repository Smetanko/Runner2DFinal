﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGeneration : MonoBehaviour {

    public GameObject ThePlatform, TheCoins;
    public Transform generationPoint;
    public Transform pointCoins;
    public float distanceBetween;
    public float distanceCoins;

    private float platformWidth;
    private float CoinWidth;

    public float distanceBetweenMin;
    public float Max;
    public float distanceBetweenMax;
    public float Min;

	// Use this for initialization
	void Start () {
        platformWidth = ThePlatform.GetComponent<BoxCollider2D>().size.x;
		
	}
	
	// Update is called once per frame
	void Update () {

        distanceBetween = Random.Range(distanceBetweenMin, distanceBetweenMax);

        if (transform.position.x<generationPoint.position.x)
        {
            transform.position = new Vector3(transform.position.x + platformWidth + distanceBetween,Random.Range(-5.0f,0.0f) /*transform.position.y*/, transform.position.z);
            ThePlatform = Instantiate(ThePlatform, transform.position, transform.rotation);
            TheCoins.transform.position = new Vector3(transform.position.x + platformWidth + distanceCoins, transform.position.y+5.0f, transform.position.z);
            TheCoins= Instantiate(TheCoins, transform.position, transform.rotation);

            

        }
		
	}
}
